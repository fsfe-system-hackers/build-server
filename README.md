<!--
SPDX-FileCopyrightText: 2020 Free Software Foundation Europe <https://fsfe.org>

SPDX-License-Identifier: GPL-3.0-or-later
-->

# FSFE Build Server

[![in docs.fsfe.org](https://img.shields.io/badge/in%20docs.fsfe.org-OK-green)](https://docs.fsfe.org/repodocs/build-server/00_README)
[![REUSE status](https://api.reuse.software/badge/git.fsfe.org/fsfe-system-hackers/build-server)](https://api.reuse.software/info/git.fsfe.org/fsfe-system-hackers/build-server)

This Ansible playbook automatically sets up the build server for the FSFE website.

Clone this repo:
``` bash
git clone --recurse-submodules git@git.fsfe.org:fsfe-system-hackers/build-server.git
```

Update the [inventory
submodule](https://git.fsfe.org/fsfe-system-hackers/inventory) to reflect the
newest changes to the list of our hosts and the groups that they are in:
``` bash
git submodule update --remote inventory
```

## Features

* Set up build server from scratch
* Enable build server to access the webserver(s)
* Clone the git repos for fsfe-website (master and test branch)
* Configure the web build status page incl. TLS cert
* Set up cronjob to make the website build automatically

## Deploy

To deploy the while playbook, just run:

```bash
ansible-playbook setup.yml \
        -l "buildserver,webserver,gitserver"
```

This will make all edits as described. Please note that you will have to be able to decrypt `vaultpw.gpg`.

## Playbook Structure

The playbook is logically split in multiple tasks, all initiated from `setup.yml`. It also includes a role copied from [webserver](https://git.fsfe.org/fsfe-system-hackers/webserver) for certbot.

All variables are defined in `group_vars/all`.

## Build server structure

The build runs as an unprivileged user, currently `build`.

All significant build files reside under `/srv/www` (or `{{ build_dir }}` respectively). There also in a crontab file and of course the Apache2 config and Let's Encrypt files.

## Notes

* There is one encrypted string and three files for the LDAP authentication to run a full build. The files are encrypted with the GPG keys of the System Hackers coordinators.
* The build server's public SSH key needs to be set as deploy key in the Git repo. Instructions will appear in the output if access to Git does not seem to be possible.
